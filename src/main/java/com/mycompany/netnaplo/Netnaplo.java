/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.netnaplo;

import com.mycompany.netnaplo.Dao.Database;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Zsolt
 */
public class Netnaplo {

    public static void init() {
        try {
            FileReader fr = new FileReader("properties.properties");
            Properties prop = new Properties();
            
            prop.load(fr);
            
            Database.setUsername(prop.getProperty("username"));
            Database.setPassword(prop.getProperty("password"));
            Database.setUrl(prop.getProperty("url"));
            
        } catch (IOException i) {
            System.out.println("Hiba történ fájl olvasás közben");
        }
    }

    public static void main(String[] args) {
        init();

        System.out.println("Fut a program!");
        Fomenu fomenu = new Fomenu();
        fomenu.fomenu();
        Database.disconnect();

    }
}
