/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.netnaplo.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zsolt
 */
public class DataFactory {

    private static Connection db = Database.getConnection();

    public void osszBejegyzes() {
        List<Bejegyzes> lista = new ArrayList<>();
        long id = 0;
        String cim = "";
        String leiras = "";
        Timestamp letrehozva;
        try {
            Statement statement = db.createStatement();
            ResultSet rst = statement.executeQuery("Select * from netnaplo.bejegyzes");
            while (rst.next()) {
                id = rst.getLong("bejegyzesId");
                cim = rst.getString("cim");
                leiras = rst.getString("leiras");
                letrehozva = rst.getTimestamp("letrehozva");

                Bejegyzes b = new Bejegyzes(id, cim, leiras, letrehozva);
                lista.add(b);
            }
        } catch (SQLException se) {
            System.out.println("Hiba történt: " + se);
        }
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i).toString());
        }

    }

    public void ujSztoriFelvitel(String cim, String tartalom) {
        String sql = "Insert into bejegyzes(cim,leiras) Values(?,?)";
        try {
            PreparedStatement statement = db.prepareStatement(sql);
            statement.setString(1, cim);
            statement.setString(2, tartalom);
            statement.executeUpdate();
            System.out.println("Sikeres beszúrás történt!");

        } catch (SQLException e) {
            System.out.println("Hiba történ beszúrás közben: " + e);
        }
    }

    public void sztoriTorles(int id) {
        String sql = "delete from netnaplo.bejegyzes where bejegyzesId like '" + id +"' ";
        try{
        PreparedStatement statement = db.prepareStatement(sql);
        statement.executeUpdate();
        }catch(SQLException s){
            System.out.println(s);
        }

    }

    public void sztoriKereses(String sztoriCim) {
        String sql = "select * from netnaplo.bejegyzes where cim like '" + sztoriCim + "' ";
        try {
            Statement st = db.createStatement();
            ResultSet rst = st.executeQuery(sql);
            while (rst.next()) {
                System.out.println("Bejegyzés ID: " + rst.getInt("bejegyzesId"));
                System.out.println("Bejegyzés címe: " + rst.getString("cim"));
                System.out.println("Bejegyzés leírás: " + rst.getString("leiras"));
                System.out.println("\n");
            }

        } catch (SQLException e) {
            System.out.println("Hiba történt: " + e);
        }
    }

}
