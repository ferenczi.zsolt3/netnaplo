/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.netnaplo.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Zsolt
 */
public class Database {
    

    private static String username;
    private static String url;
    private static String password;

    private static Connection con = null;

    public static Connection getConnection() {
        if (con == null) {
            con = connect();
        }
        return con;
    }

    public static Connection connect() {
        try {
            con = DriverManager.getConnection(url, username, password);
            System.out.println("sikeres csatlakozás!");
        } catch (SQLException ex) {
            System.out.println("Hiba: " + ex);
        }
        return con;
    }

    public static void disconnect() {
        try {
            con.close();
            System.out.println("Adatbázis lezárása sikerült");
        } catch (SQLException e) {
            System.out.println("Hiba bezárás közben " + e);
        } finally {
            con = null;
        }
    }

    public static void setUsername(String username) {
        Database.username = username;
    }

    public static void setUrl(String url) {
        Database.url = url;
    }

    public static void setPassword(String password) {
        Database.password = password;
    }

}
