/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.netnaplo.Dao;

import java.util.Date;

/**
 *
 * @author zsolt
 */
public class Bejegyzes {

    private long id;
    private String cim;
    private String leiras;
    private Date letrehozas;

    public Bejegyzes(long id, String cim, String leiras, Date letrehozas) {
        this.id = id;
        this.cim = cim;
        this.leiras = leiras;
        this.letrehozas = letrehozas;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCim() {
        return cim;
    }

    public void setCim(String cim) {
        this.cim = cim;
    }

    public String getLeiras() {
        return leiras;
    }

    public void setLeiras(String leiras) {
        this.leiras = leiras;
    }

    public Date getLetrehozas() {
        return letrehozas;
    }

    public void setLetrehozas(Date letrehozas) {
        this.letrehozas = letrehozas;
    }
    

    @Override
    public String toString() {
        return "Bejegyzes " + "id=" + id + ", cim=" + cim + ", leiras=" + leiras + ", letrehozas=" + letrehozas;
    }



}
