/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.netnaplo;

import com.mycompany.netnaplo.Dao.DataFactory;
import java.sql.Connection;
import java.util.Scanner;

/**
 *
 * @author Zsolt
 */
public class Almenu {

    private DataFactory dataFactory = new DataFactory();

    public void listazas() {
        System.out.println("Sztorik listázása: ");
        dataFactory.osszBejegyzes();

    }

    public void felvitel() {
        System.out.println("Új sztori felvitele");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Cím megadása: ");
        String cim = scanner.nextLine();
        System.out.println("Sztori megírása: ");
        String sztori = scanner.nextLine();
        dataFactory.ujSztoriFelvitel(cim, sztori);
        scanner.close();
    }

    public void modositas() {
        System.out.println("modositás!");
    }

    public void torles() {
        int sztoriId=0;
        try {
            System.out.println("Kérem a sztori ID-t: ");
            Scanner sc = new Scanner(System.in);
            String id = sc.nextLine();
            sztoriId = Integer.parseInt(id);
        } catch (NumberFormatException n) {
            System.out.println(n);
        }
        dataFactory.sztoriTorles(sztoriId);

    }

    public void kereses() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Keresés cím alapján:");
        System.out.println("Add meg a címet: ");
        String sztoriCim = sc.next();
        dataFactory.sztoriKereses(sztoriCim);
    }

}
