/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.netnaplo;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Zsolt
 */
public class Fomenu {

    public void FomenuLista() {
        System.out.println("Válassz az alábbi menüpontok közül: ");
        System.out.println("Új sztori felvitele:--> 1");
        System.out.println("Sztorik listázása:-->   2");
        System.out.println("Sztori módosítás:-->    3");
        System.out.println("Bejegyzés törlése:-->   4");
        System.out.println("Bejegyzés keresése:-->  5");
        System.out.println("Kilépés:-->             6\n");
    }

    public void fomenu() {
        int bekertszam = 0;
        Almenu almenu = new Almenu();
        do {
            bekertszam = bekeres();
            switch (bekertszam) {
                case 1:                    
                    almenu.felvitel();
                    break;
                case 2:
                    almenu.listazas();
                    break;
                case 3:
                    almenu.modositas();
                    break;
                case 4:
                    almenu.torles();
                    break;
                case 5:
                    almenu.kereses();
                    break;
                case 6:
                    System.out.println("Kilépés");
                    break;
                    default: System.out.println("Nem jó számot adtál meg");
            }
        } while (bekertszam != 6);
    }

    public int bekeres() {
        Scanner sc = new Scanner(System.in);
        int fomenuszam = 0;
        boolean helyesValasztas = false;
        do {
            try {
                FomenuLista();
                fomenuszam = sc.nextInt();
                helyesValasztas = true;
            } catch (InputMismatchException e) {
                System.out.println("Nem számot adtál meg");
                sc.next();
            }
        } while (helyesValasztas != true);
        return fomenuszam;
    }

}
